package net.kamradtfamily.asyncservice;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.TimeoutException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

/** 
* AsyncServiceApplication Tester. 
* 
* @author <Authors name> 
* @since <pre>Dec 25, 2022</pre> 
* @version 1.0 
*/
@ExtendWith(SpringExtension.class)
@WebFluxTest(AsyncServiceApplication.class)
@ContextConfiguration(classes = ReactiveElasticsearchOperations.class)
@Slf4j
public class AsyncServiceApplicationTest {
    @Mock
    ReactiveElasticsearchOperations operations;
    ObjectMapper objectMapper = new ObjectMapper();
    /**
     * Method: main(String[] args)
     */

    @org.junit.jupiter.api.Test
    void stupidTest() throws IOException {
        AsyncServiceApplication.main(new String[] {""});
    }

    @Test
    public void testProcess() throws Exception {
        AsyncServiceApplication sut = new AsyncServiceApplication(operations);
        Mockito.when(operations.save(any(JsonNode.class), eq(IndexCoordinates.of("my-index")))).thenReturn(Mono.just(
                objectMapper.createObjectNode().put("message", "success")));

        sut.eventConsumer().accept("{ \"message\": \"this is the message\" }");
        String value = sut.eventProducer().get();
        JsonNode json = objectMapper.readTree(value);
        System.out.println("found json " + json);
        assertTrue(json.hasNonNull("response"));
        assertEquals("success", json.path("response").asText(""));
    }

    @Test
    public void testBadConsumer() throws Exception {
        AsyncServiceApplication sut = new AsyncServiceApplication(operations);
        Mockito.when(operations.save(any(JsonNode.class), eq(IndexCoordinates.of("my-index")))).thenReturn(Mono.just(
                objectMapper.createObjectNode().put("message", "success")));

        sut.eventConsumer().accept("{ \"message\": }\"this is the message\" }");
        Thread.sleep(12000); // consumption delays for 10 seconds to simulate doing stuffs
        Mono<String> value = Mono.fromCallable(() -> sut.eventProducer().get())
                .subscribeOn(Schedulers.single())
                .timeout(Duration.ofSeconds(1));
        StepVerifier.create(value)
                .verifyError(TimeoutException.class);
    }
}