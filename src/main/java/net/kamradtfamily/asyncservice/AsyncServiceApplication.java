package net.kamradtfamily.asyncservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.time.Duration;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.function.Consumer;
import java.util.function.Supplier;

@SpringBootApplication
@Slf4j
public class AsyncServiceApplication {
	private final BlockingQueue<JsonNode> queue = new ArrayBlockingQueue<>(4);
	private final ObjectMapper objectMapper = new ObjectMapper();
	private final ReactiveElasticsearchOperations operations;

	public AsyncServiceApplication(ReactiveElasticsearchOperations operations) {
		this.operations = operations;
	}


	public static void main(String[] args) throws IOException {
		SpringApplication.run(AsyncServiceApplication.class, args);
		Properties git = new Properties();
		git.load(AsyncServiceApplication.class.getResourceAsStream("/git.properties"));
		git.entrySet().stream().forEach(es -> log.info("{}: {}", es.getKey(), es.getValue()));
	}

	@Bean
	public Supplier<String> eventProducer(){
		return () -> {
			try {
				JsonNode item = queue.take();
				log.info("pulled {} from queue", item);
				JsonNode resp = objectMapper.createObjectNode()
						.put("response", item.path("message").asText(""));
				return resp.toPrettyString();
			} catch (InterruptedException e) {
				log.warn("process interrupted");
				Thread.currentThread().interrupt();
				throw new IllegalStateException("process interrupted");

			}
		};
	}

	@Bean
	public Consumer<String> eventConsumer() {
		return value -> Mono.just(value)
				.flatMap(v -> {
					try {
						return Mono.just(objectMapper.readTree(v));
					} catch (JsonProcessingException e) {
						log.warn("exception deserializing {}", v);
						return Mono.error(e);
					}
				})
				.doOnNext(json -> log.info("message {} received", json))
				.filter(json -> json.hasNonNull("message"))
				.flatMap(json -> operations.save(json, IndexCoordinates.of("my-index"))) //Mono.just(json)))
				.map(queue::add)
				.delaySubscription(Duration.ofSeconds(10))
				.subscribe(i -> log.info("subscribe returned {}", i),
						e -> log.warn("subscribe returned error", e));
	}

}
